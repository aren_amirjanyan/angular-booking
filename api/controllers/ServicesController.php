<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/18/2017
 * Time: 1:50 AM
 */
require_once 'Controller.php';

class ServicesController extends Controller {

    public function __construct($action, $parameters)
    {
        parent::__construct($action,$parameters);
    }

    public function getAllServices() {
        return $this->response();
    }
}