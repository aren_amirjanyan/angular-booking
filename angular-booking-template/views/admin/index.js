/**
 * Created by Aren on 2/19/2017.
 */
var appAdmin = angular.module("BookingAppAdmin", ['ui.router','angularModalService'])
.constant('SITE_URL','http://avma-video.dev')
.constant('ADMIN_PATH','/wp-content/plugins/angular-booking/angular-booking-template/views/admin')
.constant('API_ENDPOINT','/wp-admin/admin-ajax.php');