<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/18/2017
 * Time: 2:01 AM
 */

class Controller {

    public $action;

    public $parameters;

    public $data = null;

    public $status = 'ok';

    public $error = false;

    public $message = 'Successfully';

    public function __construct($action,$parameters)
    {
        $this->action = $action;

        $this->parameters = $parameters;
    }
    public function init(){

        $actionName = $this->action;

        return $this->$actionName();
    }

    public function response(){
        $data = array(
            'data'=>$this->data,
            'status'=>$this->status,
            'error'=> $this->error,
            'message'=>$this->message
        );

        return json_encode($data);
    }
}