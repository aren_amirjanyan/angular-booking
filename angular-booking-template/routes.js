/**
 * Created by Aren on 2/19/2017.
 */
app.config(function ($stateProvider) {
    $stateProvider
        .state('step-1', {
            url: "/step-1",
            controller: 'HomeController',
            templateUrl: "/wp-content/plugins/angular-booking/angular-booking-template/views/home/step-1.html"
        });
});