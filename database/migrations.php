<?php

/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/26/2017
 * Time: 3:35 PM
 */
class Migrations
{

    private $table_prefix = 'wp_';

    private $tables = array(
        'categories'=>'categories',
        'services'=>'services',
        'appointments'=>'appointments',
        'customers'=>'customers',
        'members'=>'members',
        'payments'=>'payments',
        'coupons'=>'coupons',
        'countries'=>'countries',
        'payment_type'=>'payment_type',
        'services_members'=>'services_members',
    );

    public function __construct()
    {
        if(!empty($this->tables)){
            foreach ($this->tables as $key => $value){
                $this->tables[$key] = $this->table_prefix.$value;
            }
        }

    }
    public function migrate(){
        $this->createCategoriesTable($this->tables['categories']);
        $this->createServicesTable($this->tables['services']);
        $this->createAppointmentsTable($this->tables['appointments']);
        $this->createCustomersTable($this->tables['customers']);
        $this->createMembersTable($this->tables['members']);
        $this->createPaymentsTable($this->tables['payments']);
        $this->createCouponsTable($this->tables['coupons']);
        $this->createCountriesTable($this->tables['countries']);
        $this->createPaymentTypeTable($this->tables['payment_type']);
        $this->createServicesMembersTable($this->tables['services_members']);
    }

    public function createCategoriesTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `title` VARCHAR(255) NOT NULL,
	  `description` text NOT NULL,
	  `parent_category` int(11) DEFAULT 0 NOT NULL,
	  `created_at` int(11) NOT NULL,
	  `updated_at` int(11) NOT NULL, 
	   PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createServicesTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `title` VARCHAR(255) NOT NULL,
	  `description` text NOT NULL,
	  `before_padding_time` int(11) NOT NULL,
	  `after_padding_time` int(11) NOT NULL,
	  `price` int(11) NOT NULL,
	  `capacity` int(11) NOT NULL,
	  `category_id` int(11) NOT NULL,
	  `color` VARCHAR(255) NOT NULL,
	  `created_at` int(11) NOT NULL,
	  `updated_at` int(11) NOT NULL, 
	   PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createAppointmentsTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `booking_time` int(11) NOT NULL,
          `member_id` int(11) NOT NULL,
          `customer_id` int(11) NOT NULL,
          `service_id` int(11) NOT NULL,
          `duration` int(11) NOT NULL,
          `payment_id` int(11) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createCustomersTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `username` VARCHAR(255) NOT NULL,
          `password` VARCHAR(255) NOT NULL,
          `first_name` VARCHAR(255) NOT NULL,
          `last_name` VARCHAR(255) NOT NULL,
          `phone` VARCHAR(255) NOT NULL,
          `email` VARCHAR(255) NOT NULL,
          `notes` text NOT NULL,
          `ip_address` VARCHAR(255) NOT NULL,
          `country_code_id` int(11) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createMembersTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `first_name` VARCHAR(255) NOT NULL,
          `last_name` VARCHAR(255) NOT NULL,
          `phone` VARCHAR(255) NOT NULL,
          `email` VARCHAR(255) NOT NULL,
          `photo` VARCHAR(255) NOT NULL,
          `country_code_id` int(11) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createPaymentsTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `type` int(11) NOT NULL,
          `customer_id` int(11) NOT NULL,
          `member_id` int(11) NOT NULL,
          `service_id` int(11) NOT NULL,
          `amount` int(11) NOT NULL,
          `status` ENUM("completed","pending"),
          `coupon` VARCHAR(255) NOT NULL,
          `appointment_date` int(11) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createCouponsTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `code` VARCHAR(255) NOT NULL,
          `discount` int(3) NOT NULL,
          `deduction` int(11) NOT NULL,
          `usage_limit` int(11) NOT NULL,
          `number_of_used` int(11) NOT NULL,
          `status` int(1) DEFAULT 1 NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }

    public function createCountriesTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` VARCHAR(255) NOT NULL,
          `code` VARCHAR(255) NOT NULL,
          `phone_number` VARCHAR(255) NOT NULL,
          `iso_code` VARCHAR(255) NOT NULL,
          `population` VARCHAR(255) NOT NULL,
          `area_km` int(11) NOT NULL,
          `gdp_USD` VARCHAR(255) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createPaymentTypeTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `title` VARCHAR(255) NOT NULL,
          `description` text NOT NULL,
          `short_code` VARCHAR(255) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
    public function createServicesMembersTable($table_name)
    {
        $sqlQuery = 'CREATE TABLE ' . $table_name . ' (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `service_id` int(11) NOT NULL,
          `member_id` int(11) NOT NULL,
          `created_at` int(11) NOT NULL,
          `updated_at` int(11) NOT NULL, 
           PRIMARY KEY `id` (id)
	    );';
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sqlQuery);
    }
}