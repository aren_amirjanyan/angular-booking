<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/18/2017
 * Time: 1:50 AM
 */
require_once 'Controller.php';
require_once '/../models/Category.php';

class CategoriesController extends Controller
{

    public function __construct($action, $parameters)
    {
        parent::__construct($action, $parameters);
    }

    public function getAllCategories()
    {
        $this->data = Category::select();
        return $this->response();
    }

    public function addCategory()
    {
        $model = new Category();
        $model->title = $this->parameters['title'];
        $model->description = $this->parameters['description'];
        $model->parent_category = ($this->parameters['parent_category'])?$this->parameters['parent_category']:0;
        $model->created_at = time();
        $model->updated_at = time();
        if ($model->save()) {
            $this->data = Category::select();
        } else {
            $this->data = [];
            $this->status = '';
            $this->error = true;
            $this->message = 'Something went wrong';
        }
        return $this->response();

    }
}