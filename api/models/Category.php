<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/26/2017
 * Time: 6:49 PM
 */
require_once 'Model.php';

class Category extends Model {

    public $table = 'categories';

    static $table_prefix = 'wp_';

    static $table_name = 'categories';

    static $db;

    public $fields = array('title','description','parent_category','created_at','updated_at');

    public function __construct()
    {
        parent::__construct($this->table);

    }
    public static function select(){
        global $wpdb;
        self::$db = $wpdb;
        parent::$table_name = self::$table_prefix.self::$table_name;
        parent::$db = self::$db;
        return parent::select();
    }

}