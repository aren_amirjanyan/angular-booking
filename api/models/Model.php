<?php

/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/26/2017
 * Time: 6:12 PM
 */
class Model
{

    public $table;

    private $table_prefix = 'wp_';

    private $wpdb;

    public $attributes = array();

    public $fields = array();

    static $table_name;

    static $db;

    public function __construct($table)
    {
        global $wpdb;
        $this->table = $table;
        $this->wpdb = $wpdb;
    }

    public static function select($fields = '*',$condition = ''){
        if($condition)
            $condition ='WHERE '.$condition;
        $table_name = self::$table_name;
        $results = self::$db->get_results("SELECT $fields FROM $table_name $condition");
        return $results;
    }

    public function save()
    {
        $this->setAttributes();
        $table_name = $this->table_prefix . $this->table;
        if(!empty($this->attributes)){
            return $this->wpdb->insert(
                $table_name,
                $this->attributes
            );
        }else
            return false;
    }

    public function update()
    {

    }

    public function delete($id)
    {

    }
    private function setAttributes(){
        if (!empty($this->fields)) {
            foreach ($this->fields as $field) {
                $this->attributes[$field] = $this->$field;
            }
        }
    }

}