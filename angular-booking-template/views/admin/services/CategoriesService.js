/**
 * Created by Aren on 2/21/2017.
 */
appAdmin.service('CategoriesService', function($http,$q,$httpParamSerializer,SITE_URL,API_ENDPOINT) {

    this._http = $http;

    this._$httpParamSerializer = $httpParamSerializer;

    this.api_endpoint = SITE_URL + API_ENDPOINT;

    this.headers =  {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    this.getAllCategories = function () {

        var deferred = $q.defer();

        var data = {
            action: 'api',
            data:{
                controller:'categories',
                action:'getAllCategories',
                parameters:{}
            }
        };

        this._http({
            url: this.api_endpoint,
            method: "POST",
            data: this._$httpParamSerializer(data),
            headers: this.headers,
            dataType:'json',
        }).then(function(response){
            deferred.resolve(response);
        }).catch(function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    };
    this.addCategory = function (parameters) {

        var deferred = $q.defer();

        var data = {
            action: 'api',
            data:{
                controller:'categories',
                action:'addCategory',
                parameters:parameters
            }
        };

        this._http({
            url: this.api_endpoint,
            method: "POST",
            data: this._$httpParamSerializer(data),
            headers: this.headers,
            dataType:'json',
        }).then(function(response){
            deferred.resolve(response);
        }).catch(function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    }
});