/**
 * Created by Aren on 2/19/2017.
 */
appAdmin.controller('CategoriesController', function ($scope, $state, CategoriesService, ModalService,SITE_URL,ADMIN_PATH) {

    $scope._categoriesService = CategoriesService;

    $scope.title = "Categories";

    $scope.categories = [];

    $scope._categoriesService.getAllCategories().then(function (response) {
        if (response && response.data.status == 'ok' && response.data.data.length > 0) {
            $scope.categories = response.data.data;
            //console.log($scope.categories);
        }

    }).catch(function (error) {
        console.error(error);
    });

    $scope.openCategoryModal = function () {
        // Just provide a template url, a controller and call 'showModal'.
        ModalService.showModal({
            templateUrl: SITE_URL + ADMIN_PATH + "/modals/add-category.html",
            inputs: {
                parentScope: $scope
            },
            controller: function ($scope, parentScope, $element, close) {
                $scope.title = 'Add Category';
                $scope.categories = parentScope.categories;
                $scope.addCategory = function () {
                    var parameters = {
                        title: $scope.categoryName,
                        description: $scope.categoryDescription,
                        parent_category: $scope.parentCategory,
                    };
                    $scope.categories = parentScope._categoriesService.addCategory(parameters).then(function (response) {
                        if (response && response.data.status == 'ok' && response.data.data.length > 0) {
                            return response.data.data;
                        } else {
                            console.log(response);
                        }
                    }).catch(function (error) {
                        console.error(error)
                    });
                    $scope.close();
                };
                $scope.close = function () {
                    $element.modal('hide');
                    close($scope, 300);
                };
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.categories = result.categories;
            });
        });

    };
    $scope.showCategory = function($index){
        alert($index);
    };
    $scope.editCategory = function($index){
        alert($index);
    };
    $scope.deleteCategory = function($index){
        alert($index);
    };


});