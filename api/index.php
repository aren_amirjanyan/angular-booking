<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 2/18/2017
 * Time: 1:40 AM
 */
class Api {

    private $controller;

    private $action;

    private $parameters;

    private $controllerClassName;

    public $result;

    public function __construct($data)
    {

            $this->controller = $data['controller'];

            $this->action = $data['action'];

            $this->parameters = $data['parameters'];

            $this->controllerClassName = ucfirst($this->controller)."Controller";

            require_once "/controllers/".$this->controllerClassName.".php";

            $controller = new $this->controllerClassName($this->action,$this->parameters);

            $this->result = $controller->init();

    }
}