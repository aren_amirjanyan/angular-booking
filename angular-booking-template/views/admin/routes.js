/**
 * Created by Aren on 2/19/2017.
 */
appAdmin.config(function ($stateProvider) {
    $stateProvider
        .state('appointments', {
            url: "/appointments",
            controller: 'AppointmentsController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/appointments/index.html"
        })
        .state('categories', {
            url: "/categories",
            controller: 'CategoriesController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/categories/index.html"
        })
        .state('members', {
            url: "/members",
            controller: 'MembersController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/members/index.html"
        })
        .state('customers', {
            url: "/customers",
            controller: 'CustomersController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/customers/index.html"
        })
        .state('coupons', {
            url: "/coupons",
            controller: 'CouponsController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/coupons/index.html"
        })
        .state('services', {
            url: "/services",
            controller: 'ServicesController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/services/index.html"
        })
        .state('payments', {
            url: "/payments",
            controller: 'PaymentsController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/payments/index.html"
        })
        .state('settings', {
            url: "/settings",
            controller: 'SettingsController',
            templateUrl: "../wp-content/plugins/angular-booking/angular-booking-template/views/admin/views/settings/index.html"
        });
});