<?php
/**
 * Plugin Name: Angular Booking
 * Plugin URI: http://angular-booking.com/
 * Description: Booking of services with Angular JS.
 * Version: 1.0.0
 * Author: Aren Amirjanyan
 * Author URI: amirjanyan.aren@gmail.com
 * License: AMIRJANYAN2017
 */
class AngularBooking {

    private $api;

    private $request = array();

    private $requestData = array();

    /**
     * AngularBooking constructor.
     */
    public function __construct()
    {
        require_once '/helpers/functions.php';
        switch ($_SERVER['REQUEST_METHOD']){
            case 'POST' : {
                $this->request = $_POST;
                break;
            }
            case 'GET' : {
                $this->request = $_GET;
                break;
            }
            case 'DELETE':
                break;
            default:{
                $this->request = $_GET;
                break;
            }
        }
    }

    /**
     *
     */
    public function init(){
        add_shortcode('angular-booking',array($this,'show_booking_page'));
        add_filter( 'page_template', array($this, 'add_booking_page_template' ));
        add_action( 'admin_menu', array($this, 'angular_booking_menu_page' ));
        add_action( "wp_ajax_api", array($this,'apiCall'));
        add_action( "wp_ajax_nopriv_api", array($this,'apiCall'));
    }

    /**
     *
     */
    public function apiCall(){
        $result = null;
        $this->requestData =  json_decode($this->request['data'],true);
        if(!empty($this->requestData))
        {
            require_once '/api/index.php';
            $this->api = new Api($this->requestData);
            $result = $this->api->result;
        }
        echo $result;
        wp_die();
    }

    /**
     *
     */
    public function show_booking_page(){
        require_once '/angular-booking-template/index.html';
    }

    /**
     * @param $page_template
     * @return string
     */
    public function add_booking_page_template( $page_template )
    {
        if ( is_page( 'angular-booking' ) ) {
            $page_template = dirname( __FILE__ ) . '/angular-booking-template/index.html';
        }
        return $page_template;
    }
    /**
     * Register a custom menu page.
     */
   public function angular_booking_menu_page() {
        add_menu_page(
            __( 'Custom Menu Title', 'textdomain' ),
            'Angular Booking',
            'manage_options',
            'angular-booking-admin',
            array($this,'angular_booking_admin'),
            plugins_url('/angular-booking/angular-booking-template/images/icon.png' ),
            6
        );
    }

    /**
     *
     */
    public function angular_booking_admin(){
        require_once '/angular-booking-template/views/admin/index.html';
    }
}

/**
 * migration functions of plugin
 */
function angular_booking_migration_tables(){
    require_once '/database/migrations.php';
    $migrations = new Migrations();
    $migrations->migrate();
}

/**
 * activation function of plugin
 */
function functionsForActivationOfPlugin(){
    angular_booking_migration_tables();
}
/**
 * deactivation function of plugin
 */
function functionsForDeactivationOfPlugin(){

}

/**
 * Activation hook of plugin
 */
register_activation_hook( __FILE__, 'functionsForActivationOfPlugin');

/**
 * deactivation hook of plugin
 */
register_deactivation_hook( __FILE__, 'functionsForDeactivationOfPlugin' );

/**
 * run the plugin
 */
$angularBooking = new AngularBooking();
$angularBooking->init();
